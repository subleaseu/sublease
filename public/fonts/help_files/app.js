var module = angular.module('subleaseU',
['ui.router']).
config(function($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('/', {
            url: '/',
            templateUrl: 'views/index.html',
            controller: 'indexCtrl'
        })
        .state('help', {
            url: '/help',
            templateUrl: 'partials/partial-help.html'
        })
        .state('help.head', {
            url: '/help',
            templateUrl: 'partials/partial-help-head.html'
        })
        .state('edit', {
            url: '/edit',
            templateUrl: 'partials/partial-help-edit.html'
        })
        .state('edit.profile', {
            url: '/edit-profile',
            templateUrl: 'partials/partial-help-edit-profile.html'
        })
        .state('edit.verify', {
            url: '/edit-verify',
            templateUrl: 'partials/partial-help-edit-verify.html'
        })
        .state('edit.reference', {
            url: '/edit-reference',
            templateUrl: 'partials/partial-help-edit-reference.html'
        })
        .state('problem', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem.html'
        })
        .state('payment', {
            url: '/payment',
            templateUrl: 'partials/partial-help-payment.html'
        })
        .state('offer', {
            url: '/special-offer',
            templateUrl: 'partials/partial-help-offer.html'
        })
        .state('tax', {
            url: '/tax',
            templateUrl: 'partials/partial-help-tax.html'
        })
        .state('tax.works', {
            url: '/tax-how-it-works',
            templateUrl: 'partials/partial-help-tax-works.html'
        })
        .state('tax.local', {
            url: '/local-tax',
            templateUrl: 'partials/partial-help-tax-local.html'
        })
        .state('tax.federal', {
            url: '/federal-tax',
            templateUrl: 'partials/partial-help-tax-federal.html'
        })
        .state('tax.valueAdded', {
            url: '/value-added-tax',
            templateUrl: 'partials/partial-help-tax-valueAdded.html'
        })
        .state('travelling', {
            url: '/travelling',
            templateUrl: 'partials/partial-help-travelling.html'
        })
        .state('booking', {
            url: '/booking',
            templateUrl: 'partials/partial-help-booking.html'
        })
        .state('cancel', {
            url: '/cancel',
            templateUrl: 'partials/partial-help-cancel.html'
        })
        .state('terms', {
            url: '/terms-conditions',
            templateUrl: 'partials/partial-help-terms.html'
        })
        .state('reviews', {
            url: '/reviews',
            templateUrl: 'partials/partial-help-reviews.html'
        })
        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'partials/partial-dashboard.html'
        })
        .state('inbox', {
            url: '/inbox',
            templateUrl: 'partials/partial-inbox.html'
        })
        .state('property', {
            url: '/property',
            templateUrl: 'partials/partial-property.html'
        })
        .state('prospective', {
            url: '/prospective',
            templateUrl: 'partials/partial-prospective.html'
        })
        .state('prospective-invite', {
            url: '/invite-friends',
            templateUrl: 'partials/partial-prospective-invite.html'
        })
        .state('profile', {
            url: '/profile',
            templateUrl: 'partials/partial-profile.html'
        })
        .state('profile.edit', {
            url: '/profile-edit',
            templateUrl: 'partials/partial-profile-edit.html'
        })
        .state('profile.trust', {
            url: '/profile-trust-and-verification',
            templateUrl: 'partials/partial-profile-trust.html'
        })
        .state('profile.reviews', {
            url: '/profile-reviews',
            templateUrl: 'partials/partial-profile-reviews.html'
        })
        .state('profile.references', {
            url: '/profile-references',
            templateUrl: 'partials/partial-profile-references.html'
        })
        .state('account', {
            url: '/account',
            templateUrl: 'partials/partial-account.html'
        })
        .state('account.notification', {
            url: '/notifications',
            templateUrl: 'partials/partial-account-notification.html'
        })
        .state('account.paymentMethods', {
            url: '/patment-methods',
            templateUrl: 'partials/partial-account-payment-methods.html'
        })
        .state('account.paymentPreferences', {
            url: '/payment-preferences',
            templateUrl: 'partials/partial-account-payment-preferences.html'
        })
        .state('account.transaction', {
            url: '/transaction-history',
            templateUrl: 'partials/partial-account-transaction-history.html'
        })
        .state('account.transaction.completed', {
            url: '/completed',
            templateUrl: 'partials/partial-account-transaction-completed.html'
        })
        .state('account.transaction.future', {
            url: '/future',
            templateUrl: 'partials/partial-account-transaction-future.html'
        })
        .state('account.transaction.earnings', {
            url: '/earnings',
            templateUrl: 'partials/partial-account-transaction-earnings.html'
        })
        .state('account.privacy', {
            url: '/privacy',
            templateUrl: 'partials/partial-account-privacy.html'
        })
        .state('account.security', {
            url: '/security',
            templateUrl: 'partials/partial-account-security.html'
        })
        .state('account.connected', {
            url: '/connected-apps',
            templateUrl: 'partials/partial-account-connected.html'
        })
        .state('account.settings', {
            url: '/settings',
            templateUrl: 'partials/partial-account-settings.html'
        })
        .state('account.badges', {
            url: '/badges',
            templateUrl: 'partials/partial-account-badges.html'
        })
        .state('signin', {
            url: '/signin',
            templateUrl: 'partials/signin.html'
        })
        .state('signup', {
            url: '/signup',
            templateUrl: 'partials/signup.html'
        })
        .state('search', {
            url: '/search',
            templateUrl: 'partials/partial-search-result.html'
        });

        
});


module.directive('footer', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element. 
        replace: true,
        templateUrl: "js/directives/footer.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});


module.directive('header', function () {
    return {
        restrict: 'A', //This menas that it will be used as an attribute and NOT as an element.
        replace: true,
        scope: {user: '='}, // This is one of the cool things :). Will be explained in post.
        templateUrl: "js/directives/header.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});



function HeaderCtrl($scope) {
    $scope.header = {name: "header.html", url: "js/directives/header.html"};
}