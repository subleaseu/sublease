angular.module('subAdmin',[]).controller('myCtrl',function($scope,$http){

    
    function initialize() {
        
                var address = (document.getElementById('my_address'));
                var autocomplete = new google.maps.places.Autocomplete(address);
                autocomplete.setTypes(['geocode']);
                google.maps.event.addListener(autocomplete, 'place_changed', function() {
                var place = autocomplete.getPlace();
                if (!place.geometry) {
                
                    return;
                
                }

                var address = '';
                if (place.address_components) {
                    address = [
                        (place.address_components[0] && place.address_components[0].short_name || ''),
                        (place.address_components[1] && place.address_components[1].short_name || ''),
                        (place.address_components[2] && place.address_components[2].short_name || '')
                        ].join(' ');
                }
                });
            }
            
           google.maps.event.addDomListener(window, 'load', initialize);
    
    
   
    
    
    
     $scope.preference = ["Male", "Female", "No preference"];
	$scope.number = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
	$scope.type = ["Apartment",
					"Duplex",
					"House",
					"Townhome",]
	$scope.amenities = [ "All Filters",
								    "Electricity",
								    "Gas",
								    "Water",
								    "Cable",
								    "WiFi",
								    "Connected Bathroom",
								    "Shared Bathroom",
								    "Furnished",
								    "Close to Transit",
								    "Yard",
								    "Gym",
								    "Pool",
								    "Pet Friendly"];
    $scope.complex = ["Add a Complex", "Complex1", "Complex2", "Complex3"];
    $scope.safetyFeatures = ["Smoke Detector", "Carbon monoxide detector", "Fire Extiguinsher"];
    
    
    $scope.addnow=function(){
        
        console.log($scope.user);
        
        $http.post('/users/getyourlisting',$scope.user)
        .success(function(data) {
     
            console.log("Success:: "+data);
    
        }).error(function(data, status) {            
        
            console.log(data+"Status::"+status);
                
        });
        
        
        
    }
    


}).controller('mainCtrl',function($scope,$http){
 
    
     
    $scope.shownow = function(){
		
		$http.post('/users/getyourlisting')
        .success(function(data) {
			$scope.user = data;
			
            console.log($scope.user);
            console.log("Success:: "+data);
    
        }).error(function(data, status) {            
        
            console.log(data+"Status::"+status);
                
        });
	}



    $scope.showuser = function(){
        
        $http.post('/users/getusers').success(function(data){
        
            $scope.Udata=data;
            
            console.log($scope.Udata);
            
        
        }).error(function(data, status) {            
       
           console.log(data+"Status::"+status);
               
       });
    }



        
});