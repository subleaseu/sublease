var module = angular.module('subleaseU',
['ui.router','naif.base64']).
config(function($stateProvider, $urlRouterProvider) {
//    $urlRouterProvider.otherwise('/home');
    $stateProvider
        .state('/', {
            url: '/',
            templateUrl: 'index.html',
            // controller: 'indexCtrl'
        })
        .state('main', {
            url: '/main',
            templateUrl: 'main.html'
        })
        .state('help', {
            url: '/help',
            templateUrl: 'partials/partial-help.html'
        })
        .state('help.head', {
            url: '/help-center',
            templateUrl: 'partials/partial-help-head.html'
        })
        .state('edit', {
            url: '/edit',
            templateUrl: 'partials/partial-help-edit.html'
        })
        .state('edit.profile', {
            url: '/edit-profile',
            templateUrl: 'partials/partial-help-edit-profile.html'
        })
        .state('edit.verify', {
            url: '/edit-verify',
            templateUrl: 'partials/partial-help-edit-verify.html'
        })
        .state('edit.reference', {
            url: '/edit-reference',
            templateUrl: 'partials/partial-help-edit-reference.html'
        })
        .state('problem', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem.html'
        })
        .state('problem.preparing', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem-preparing.html'
        })
        .state('problem.duringTrip', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem-duringTrip.html'
        })
        .state('problem.change', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem-change.html'
        })
        .state('problem.resolve', {
            url: '/report-a-problem',
            templateUrl: 'partials/partial-help-problem-resolve.html'
        })
        .state('payment', {
            url: '/payment',
            templateUrl: 'partials/partial-help-payment.html'
        })
        .state('offer', {
            url: '/special-offer',
            templateUrl: 'partials/partial-help-offer.html'
        })
        .state('tax', {
            url: '/tax',
            templateUrl: 'partials/partial-help-tax.html'
        })
        .state('tax.works', {
            url: '/tax-how-it-works',
            templateUrl: 'partials/partial-help-tax-works.html'
        })
        .state('tax.local', {
            url: '/local-tax',
            templateUrl: 'partials/partial-help-tax-local.html'
        })
        .state('tax.federal', {
            url: '/federal-tax',
            templateUrl: 'partials/partial-help-tax-federal.html'
        })
        .state('tax.valueAdded', {
            url: '/value-added-tax',
            templateUrl: 'partials/partial-help-tax-valueAdded.html'
        })
        .state('travelling', {
            url: '/travelling',
            templateUrl: 'partials/partial-help-travelling.html'
        })
        .state('booking', {
            url: '/booking',
            templateUrl: 'partials/partial-help-booking.html'
        })
        .state('cancel', {
            url: '/cancel',
            templateUrl: 'partials/partial-help-cancel.html'
        })
        .state('terms', {
            url: '/terms-conditions',
            templateUrl: 'partials/partial-help-terms.html'
        })
        .state('reviews', {
            url: '/reviews',
            templateUrl: 'partials/partial-help-reviews.html'
        })
        
        .state('hostGuarantee', {
            url: '/hostGuarantee',
            templateUrl: 'partials/partial-footer-hostGuarantee.html'
        })

        .state('dashboard', {
            url: '/dashboard',
            templateUrl: 'partials/partial-dashboard.html'
        })
        .state('inbox', {
            url: '/inbox',
            templateUrl: 'partials/partial-inbox.html'
        })
        .state('property', {
            url: '/property',
            templateUrl: 'partials/partial-property.html'
        })
        .state('prospective', {
            url: '/prospective',
            templateUrl: 'partials/partial-prospective.html'
        })
        .state('prospective-invite', {
            url: '/invite-friends',
            
            templateUrl: 'partials/partial-prospective-invite.html'
        })
        .state('hstos', {
            url: '/hsTermOfService',
            templateUrl: 'partials/partial-footer-hs-tos.html'
        })
    
        .state('profile', {
            url: '/profile',
            templateUrl: 'partials/partial-profile.html'
        })
        .state('profile.edit', {
            url: '/profile-edit',
            templateUrl: 'partials/partial-profile-edit.html'
        })
        .state('profile.trust', {
            url: '/profile-trust-and-verification',
            templateUrl: 'partials/partial-profile-trust.html'
        })
        .state('profile.reviews', {
            url: '/profile-reviews',
            templateUrl: 'partials/partial-profile-reviews.html'
        })
        .state('profile.references', {
            url: '/profile-references',
            templateUrl: 'partials/partial-profile-references.html'
        })
        .state('profileView', {
            url: '/profileView',
            templateUrl: 'partials/partial-view-profile.html'
        })
        .state('account', {
            url: '/account',
            templateUrl: 'partials/partial-account.html'
        })
        .state('account.notification', {
            url: '/notifications',
            templateUrl: 'partials/partial-account-notification.html'
        })
        .state('account.paymentMethods', {
            url: '/patment-methods',
            templateUrl: 'partials/partial-account-payment-methods.html'
        })
        .state('account.paymentPreferences', {
            url: '/payment-preferences',
            templateUrl: 'partials/partial-account-payment-preferences.html'
        })
        .state('account.transaction', {
            url: '/transaction-history',
            templateUrl: 'partials/partial-account-transaction-history.html'
        })
        .state('account.transaction.completed', {
            url: '/completed',
            templateUrl: 'partials/partial-account-transaction-completed.html'
        })
        .state('account.transaction.future', {
            url: '/future',
            templateUrl: 'partials/partial-account-transaction-future.html'
        })
        .state('account.transaction.earnings', {
            url: '/earnings',
            templateUrl: 'partials/partial-account-transaction-earnings.html'
        })
        .state('account.privacy', {
            url: '/privacy',
            templateUrl: 'partials/partial-account-privacy.html'
        })
        .state('account.security', {
            url: '/security',
            templateUrl: 'partials/partial-account-security.html'
        })
        .state('account.connected', {
            url: '/connected-apps',
            templateUrl: 'partials/partial-account-connected.html'
        })
        .state('account.settings', {
            url: '/settings',
            templateUrl: 'partials/partial-account-settings.html'
        })
        .state('account.badges', {
            url: '/badges',
            templateUrl: 'partials/partial-account-badges.html'
        })
        .state('signin', {
            url: '/signin',
            templateUrl: 'partials/signin.html'
        })
        .state('signup', {
            url: '/signup',
            templateUrl: 'partials/signup.html'
        })
       .state('search', {
            url: '/search/:location/:property/:Numbers/:roommate/:amenities/:startDate/:endDate/:flag',
            templateUrl: 'partials/partial-search-result.html'
        })
        .state('university', {
            url: '/university/:id',
            templateUrl: 'partials/partial-search-university.html'
        })
        .state('wishlist', {
            url: '/wishlist',
            templateUrl: 'partials/partial-wishlist.html'
        })
        .state('about', {
            url: '/about-us',
            templateUrl: 'partials/partial-about.html'
        })
        .state('press', {
            url: '/press',
            templateUrl: 'partials/partial-press.html'
        })
        .state('careers', {
            url: '/careers',
            templateUrl: 'partials/partial-careers.html'
        })
        .state('blog', {
            url: '/blog',
            templateUrl: 'partials/partial-blog.html'
        })
        .state('policies', {
            url: '/policies',
            templateUrl: 'partials/partial-footer-policies.html'
        })
        .state('terms-privacy', {
            url: '/terms-privacy',
            templateUrl: 'partials/partial-footer-terms.html'
        })
        .state('disclaimer', {
            url: '/disclaimer',
            templateUrl: 'partials/partial-footer-disclaimer.html'
        })
        .state('promises', {
            url: '/promises',
            templateUrl: 'partials/partial-footer-promises.html'
        })
        .state('why-sublease', {
            url: '/subleasing',
            templateUrl: 'partials/partial-footer-why-sublease.html'
        })
        .state('why-sublease-out', {
            url: '/subleasing-out',
            templateUrl: 'partials/partial-footer-why-sublease-out.html'
        })
        .state('responsible', {
            url: '/responsible-subleasing',
            templateUrl: 'partials/partial-footer-responsible.html'
        })
        .state('responsible-out', {
            url: '/responsible-subleasing-out',
            templateUrl: 'partials/partial-footer-responsible-out.html'
        })
        .state('home-safety', {
            url: '/home-safety',
            templateUrl: 'partials/partial-footer-home-safety.html'
        })
        .state('home-safety-out', {
            url: '/home-safety-out',
            templateUrl: 'partials/partial-footer-home-safety-out.html'
        })
        .state('home-tc', {
            url: '/home-safety',
            templateUrl: 'partials/partial-footer-home-safety-tc.html'
        })

        .state('help-center', {
            url: '/home-center',
            templateUrl: 'partials/partial-footer-help-center.html'
        })
        .state('instant-book', {
            url: '/instant-booking',
            templateUrl: 'partials/partial-footer-instant-book.html'
        })
        .state('instant-book-out', {
            url: '/instant-booking-out',
            templateUrl: 'partials/partial-footer-instant-book-out.html'
        })
        .state('nav-main', {
            url: '/nav-main',
            templateUrl: 'partials/header-main-profile.html'
        })
        .state('nav-footer', {
            url: '/nav',
            templateUrl: 'partials/header-main-footer.html'
        })
        .state('property.post', {
            url: '/post-a-property',
            templateUrl: 'partials/partial-property-start-steps.html'
        })
        .state('property.post.head', {
            url: '/post-a-property-steps',
            templateUrl: 'partials/partial-property-step-head.html'
        })
        .state('property.post.head.property-step-1', {
            url: '/step-1',
            templateUrl: 'partials/partial-property-step-1.html'
        })
         .state('property.post.head.property-step-2', {
            url: '/step-2/:id',
            templateUrl: 'partials/partial-property-step-2.html'
        })
        .state('property.post.head.property-step-3', {
            url: '/step-3/:id',
            templateUrl: 'partials/partial-property-step-3.html'
        })
        .state('property.post.head.property-step-4', {
            url: '/step-4/:id',
            templateUrl: 'partials/partial-property-step-4.html'
        })
        .state('congratulations', {
            url: '/congratulations',
            templateUrl: 'partials/partial-congrats.html'
        });

        
});


module.directive('footer', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element. 
        replace: true,
        templateUrl: "js/directives/footer.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});


module.directive('footerIndexOut', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element. 
        replace: false,
        templateUrl: "js/directives/footer-index-out.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});

module.directive('footerIndex', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element. 
        replace: false,
        templateUrl: "js/directives/footer-index.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});


module.directive('header', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element.
        replace: false,
        scope: {user: '='}, 
        templateUrl: "js/directives/header.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});


module.directive('headerHelp', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element.
        replace: false,
        scope: {user: '='}, 
        templateUrl: "js/directives/header-help.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            // Your behaviour goes here :)
        }]
    }
});

module.directive('headerMain', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element.
        replace: false,
        scope: {user: '@'}, 
        templateUrl: "js/directives/header-main.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            $scope.user = {
                main: "../partials/header-main-profile.html"
            };
        }]
    }
});

module.directive('headerMainn', function () {
    return {
        restrict: 'A', //This means that it will be used as an attribute and NOT as an element.
        replace: false,
        scope: {user: '@'}, 
        templateUrl: "js/directives/index-header.html",
        controller: ['$scope', '$filter', function ($scope, $filter) {
            
        }]
    }
});

function HeaderCtrl($scope) {
    $scope.header = {name: "header.html", url: "js/directives/header.html"};
}