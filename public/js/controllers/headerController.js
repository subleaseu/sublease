module.controller('loggCtrl', function($scope,$http,$state,$location,$window) {

        // console.log("loggCtrl is calling");

        $scope.signIn = null;

        $http.post('/users/isLoggedIn')
            .success(function(data) {
            	if(data==1){

                    $scope.signIn=1;
                }
               	
           		else{

                    $scope.signIn=0;
                    $window.location='index.html';
                
                }
           		



            }).error(function(err) {

                console.log(err);

            });
});


module.controller('mainHeaderCtrl', ['$scope', '$location', function($scope, $location) {

    $scope.currentPath = $location.path();

}]);

module.controller('HeaderHelpCtrl', ['$scope', function($scope) {
    $scope.myVar = true;
}]);

module.controller('mainHeadCtrl', ['$scope', '$http', function($scope, $http) {

    $scope.header = function() {
        return 1;
    }

}]);



module.controller('searchPlacesCtrl', function($scope, $http, $state) {



    /**

        * @param[] - calling initialize after 5 seconds 


    */

    function initialize() {

        var address = (document.getElementById('my_address'));
        var autocomplete = new google.maps.places.Autocomplete(address);
        autocomplete.setTypes(['geocode']);
        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {

                return;

            }

            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });
    }

    /*
            $scope.codeAddress=function() {
				
                geocoder = new google.maps.Geocoder();
                var address = document.getElementById("my_address").value;
                geocoder.geocode( { 'address': address}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {

                  $scope.lat=("Latitude: "+results[0].geometry.location.lat());
                  $scope.longi=("Longitude: "+results[0].geometry.location.lng());
                      
					 // $scope.funct($scope.lat,$scope.longi); 
					  
                    
                  } 

                  else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });
				 
			 
			
			}
            
    */

    google.maps.event.addDomListener(window, 'load', initialize);


    $scope.University = function() {

        //        console.log(document.getElementById('my_address').value);

        $state.go('search', { 'location': document.getElementById('my_address').value, 'flag': 1 });


    }

});

module.controller('profileCtrl', function($scope, $http, $location) {


    $(document).ready(function() {


        var btnCust = '';

        $("#avatar-1").fileinput({

            overwriteInitial: true,
            maxFileSize: 1500,
            showClose: false,
            showCaption: false,
            browseLabel: 'Browse',
            removeLabel: 'Remove',
            browseIcon: '<i class="glyphicon glyphicon-folder-open"></i>',
            removeIcon: '<i class="glyphicon glyphicon-remove"></i>',
            removeTitle: 'Cancel or reset changes',
            elErrorContainer: '#kv-avatar-errors-1',
            msgErrorClass: 'alert alert-block alert-danger',
            defaultPreviewContent: '<img src="/profileupload/default_avatar_male.png" class="img-circle img-responsive" align="middle" alt="Your Avatar" id="profilePic" ng-model="profilePic" name="profilePic" style="width:160px" base-sixty-four-input>',
            layoutTemplates: { main2: '{preview} ' + '{remove} {browse}' },
            allowedFileExtensions: ["jpg", "png", "gif"]

        });





    });


    $http.post('/users/getdetails')
        .success(function(data) {

            $scope.getdetails = data;

            //console.log("sadas");

            console.log($scope.getdetails);

            //$state.go('profile.edit'); 

        })
        .error(function(err) {

            console.log(err);


        });

    $scope.profileupload = function() {

        console.log('Calling profileupload Pic');


        $scope.pbase64 = {

            data: $('.kv-preview-data.file-preview-image').attr('src')
        };

        //onsole.log($scope.pbase64.data);


        $http.post('/users/profilepics', $scope.pbase64).then(function() {


            console.log("Profile Pics Uploaded !");

        });


    }


    $scope.addpost = function() {


        console.log("<<<<Calling ADD POST>>>>");

        console.log($scope.getdetails);

        $http.post('users/editprofile', $scope.getdetails).then(function() {

            console.log($scope.getdetails);

            $location.path('profileView');


            console.log('Blog created.');


        });

        console.log('at post calling');

    }

});

module.controller('profileCtrl1', function($scope, $http, $location) {

    //$scope.getdetails;
    console.log('calling from controller 1');

    $http.post('/users/getdetails')
        .success(function(data) {

            $scope.getdetails = data;

            console.log("sadas");

            console.log($scope.getdetails);

            //$state.go('profile.edit'); 

        })
        .error(function(err) {
            console.log(err);
        });
});


/*module.controller('profileCtrl', function($scope,$http,mainService,$location){

	  $scope.users = {};
	  $scope.addPost = function() {
        mainService.addPost($scope.users).then(function() {
        $scope.users = {};
        $location.path('profileView');
        console.log('Blog created.');     
    }); 
}
});*/

/*module.service('mainService',function($http){
	//var users = {};
	this.addPost = function(users){
		return $http.post('/users/editprofile',users);
	}
});*/

/*module.controller('profile1Ctrl',function($scope,$http){
	$scope.load= function(){
		console.log($scope.user)
      var data = $.param($scope.user);
      $http.post("/users/editprofile", data).success(function(data, status) {
        console.log('Data posted successfully');
		//$state.go('property.property.post.head.property-step-2');
      })
	}
});*/

// module.controller('MapCtrl', function($scope){

// 	console.log('MapCtrl is Calling.....');


// 	// function initMap() {

//  //        console.log('Google Map Calling Me');

//  //        var map = new google.maps.Map(document.getElementById('Gmap'), {
//  //          center: {lat: -34.397, lng: 150.644},
//  //          zoom: 6
//  //        });
//  //        var infoWindow = new google.maps.InfoWindow({map: map});

//  //        // Try HTML5 geolocation.
//  //        if (navigator.geolocation) {
//  //          navigator.geolocation.getCurrentPosition(function(position) {
//  //            var pos = {
//  //              lat: position.coords.latitude,
//  //              lng: position.coords.longitude
//  //            };

//  //            infoWindow.setPosition(pos);
//  //            infoWindow.setContent('Location found.');
//  //            map.setCenter(pos);
//  //          }, function() {
//  //            handleLocationError(true, infoWindow, map.getCenter());
//  //          });
//  //        } else {
//  //          // Browser doesn't support Geolocation
//  //          handleLocationError(false, infoWindow, map.getCenter());
//  //        }
//  //      }

//  //      function handleLocationError(browserHasGeolocation, infoWindow, pos) {
//  //        infoWindow.setPosition(pos);
//  //        infoWindow.setContent(browserHasGeolocation ?
//  //                              'Error: The Geolocation service failed.' :
//  //                              'Error: Your browser doesn\'t support geolocation.');
//  //        }





// });


module.controller('propertyCtrl', function($scope, $http, $stateParams) {

    console.log("PropertyCtrl Calling ME...");

    $scope.data = {

        location: $stateParams.location,
        roommate: $stateParams.roommate,
        number: $stateParams.Numbers,
        property: $stateParams.property,
        amenities: $stateParams.amenities,
        startDate: $stateParams.startDate,
        endDate: $stateParams.endDate
    };


    //console.log($scope.data);


    if ($stateParams.flag == 1) {

        console.log("Calling From Bar");

        $http.post('/users/getlistingBar', $scope.data)
            .success(function(data) {

                $scope.listing = data;
                console.log("sadas");
                console.log($scope.listing);
            })
            .error(function(err) {
                console.log(err);
            });

    } else {

        console.log('Calling From getListing');
        $http.post('/users/getlisting', $scope.data)
            .success(function(data) {

                $scope.listing = data;
				var datas = $scope.listing.length;
				console.log($scope.listing.length);
                console.log($scope.listing);

                //console.log( $scope.listing[0]['upload_photo'][0] );
            })
            .error(function(err) {
                console.log(err);
            });
    }

});

module.controller('showproperty', function($scope, $http, $stateParams, $window) {

    $scope.id = $stateParams.id;

    console.log($scope.id);

    $http.post('/users/showlisting/' + $scope.id)
        .success(function(data) {

            $scope.listing = data;
            console.log("sadassdasdada");
            console.log(data);
        })
        .error(function(err) {
            console.log(err);
        });

    $scope.notify = function() {
        console.log($scope.id);
        $http.post('/users/notification/' + $scope.id)
            .success(function(data) {
                $window.alert('Your request has been sent');
                console.log("notify me");
                console.log(data);
            })
            .error(function(err) {
                console.log(err);
            });
    }

});



module.controller('yourproperty', function($scope,$http) {
    // console.log('heyyyy!!!');
    
    $scope.myproperty = function(){
     
    $http.post('/users/myproperty')
    .success(function(data){
        $scope.user = data;
        console.log($scope.user);
        // console.log('sssssss');
    }).error(function(data, status){
        console.log(data+"Status::"+status);
    });

    }
});

module.controller('notifies', function($scope, $http, $state, $stateParams) {

    console.log('notify me');

    $scope.notification = function() {
        $http.post('/users/notificationcount')
            .success(function(data) {
                console.log("notify");
                //console.log(data);

                $state.go('dashboard');
            })
            .error(function(err) {
                console.log(err);
            });
    }

});


