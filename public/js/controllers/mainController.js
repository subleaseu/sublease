module.controller('mainCtrl', function($scope) {

    $scope.complex = ["Add a Complex",
    					 "Complex1",
    					  "Complex2",
    					   "Complex3"];
    $scope.refundTime = ["1-Month",
    						 "2-Weeks",
    						  "7-Days",
    						   "1-Day",
    						    "12-hours",
    						     "From the start date"];
    $scope.country = ["India", "abc", "xyz"];

});


module.controller('highchartsCtrl', function($scope) {

	Highcharts.chart('highchart', {

					chart: {
          			  	type: 'bar'
        			},

				    xAxis: {
				        categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 
				            'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
				    },

				    yAxis: {
			            title: {
			                text: 'Price per month'
			            }
			        },

				    series: [{
				        data: [29.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]
				    }]
				});

});

module.controller("P_request",function($scope){

	$scope.hidder=false;
	$scope.ctrl=false;

	$scope.request=function(){

		$scope.hidder=!$scope.hidder;
	};

	$scope.RemoveEle=function(){

		$scope.ctrl=!$scope.ctrl;

	}

});

module.controller("complexctrl",function($scope){

	$scope.hideme=function(){
		
		$('#complex').hide(200);
		$('#duplex').hide(200);

	}


});



module.controller('cinvite', function($scope,$http) {

 	$http.get("emaillist.json").then(function(response) {
        
        $scope.roles = response.data.records;

        console.log($scope.roles);
    
    });
  
  $scope.user = {
    
    roles: ['Email']

  };
  // $scope.abc = function(){

  // }

});

module.run(function($rootScope,$state) {

	$rootScope.fun=function(){

		//var url = $state.href(cont, {absolute: true});
		window.open('partials/partial-email-list-small.html',"RankME","height=500,width=600")


	}
    
});






// -------------JQUERY------------------

// $(function() {
//     $('#datetimepicker').datetimepicker({
//       pickTime: false
//     });
//   });