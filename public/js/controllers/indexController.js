module.controller('searchCtrl', ["$scope", "$http", "$state", "$stateParams", function($scope, $http, $state, $stateParams) {



    initialize();


    /**
        Turn on/off Developer mode
        @type{boolean} - ADMIN_MODE
    
    */

    var ADMIN_MODE = true;

    /**

            @type{number} - lat
            @type{number} - longi
            @type{string} - loc
            @type{string} - roommate
            @type{number} - pro_type
            @type{number} - No. of roommates
            @type{string} - Amenities_2
            @type{string} - base64
               
    */

    //          var map;
    //          var infowindow;

    var imageLoader, canvas, ctx;
    $scope.lat = null;
    $scope.longi = null;
    $scope.loc = null;
    $scope.roommate = null;
    $scope.pro_type = null;
    $scope.no = null;
    $scope.amenities_2 = null;
    $scope.base64;



    /**

        This Document will bypass the enter key


    */

    /**
        This Function Will Upload Images In Step2 
        
        * @param{} fileinput-upload-button - upload images on Step2 

    */


    $(document).ready(function() {

        $(".frpgroup").hide();

        var userFlag = 0;

        $("#frg").click(function() {

            console.log("IHIHIHIHIH");

            if (userFlag == 0) {

                $(".frpgroup").show();
                userFlag = 1;
            } else {
                $(".frpgroup").hide();
                userFlag = 0;
            }

        });

        $("#userPhoto").fileinput({ showCaption: false });

        $('#my_address').keypress(function(e) {

            if (e.which == 13) {
                e.preventDefault();
            }

        });

        $('#my_address_step1').keypress(function(e) {

            if (e.which == 13) {
                e.preventDefault();
            }

        });


        $(".fileinput-upload-button").click(function() {

            var photoJ = $scope.userPhoto;

            if (ADMIN_MODE)
                console.log(photoJ);

            $.post("/users/pic", { photoJ }, function(data, status) {

                if (ADMIN_MODE)
                    console.log("DATA" + data);

            });


        });



    });









    /**
                    
            * This Function is Used in Google Places
            * Calling intialize after 5 seconds
            * @param {} - Intialize the Google Places
    */


    function initialize() {

        var address = (document.getElementById('my_address'));
        var address2 = (document.getElementById('my_address_step1'));
        var autocomplete = new google.maps.places.Autocomplete(address);
        var autocomplete2 = new google.maps.places.Autocomplete(address2);
        autocomplete.setTypes(['geocode']);
        autocomplete2.setTypes(['geocode']);

        google.maps.event.addListener(autocomplete, 'place_changed', function() {
            var place = autocomplete.getPlace();
            if (!place.geometry) {

                return;

            }

            var address = '';
            var address2 = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });

        google.maps.event.addListener(autocomplete2, 'place_changed', function() {
            var place = autocomplete2.getPlace();
            if (!place.geometry) {

                return;

            }

            var address = '';
            var address2 = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
        });

    };

    /* $scope.codeAddress=function() {
                
                geocoder = new google.maps.Geocoder();
                var address = document.getElementById("my_address").value;
                geocoder.geocode( { 'address': address}, function(results, status) {
                  if (status == google.maps.GeocoderStatus.OK) {

                  $scope.lat=("Latitude: "+results[0].geometry.location.lat());
                  $scope.longi=("Longitude: "+results[0].geometry.location.lng());
                      
                     // $scope.funct($scope.lat,$scope.longi); 
                      
                    
                  } 

                  else {
                    alert("Geocode was not successful for the following reason: " + status);
                  }
                });
                 
             
            
            }*/

    google.maps.event.addDomListener(window, 'load', initialize);


    /**
        
            * It Will get the Updated Values from half-opaque container div
            * @param{} - Get the updated value from half-opaque
        
    */

    $scope.locations = function() {

        $scope.roommate;
        $scope.no;
        $scope.pro_type;
        $scope.amenities_2;
        $scope.startDate;
        $scope.endDate;

        $scope.loc = document.getElementById('my_address').value;

        var dateSplit = $scope.date.split('-');

        $scope.startDate = dateSplit[0];
        $scope.endDate = dateSplit[1];

        if (ADMIN_MODE) {

            console.log("StartDate::_" + dateSplit[0] + "\n" + "EndDate::_" + dateSplit[1]);
            console.log("Address:: " + $scope.loc);
            console.log("Address:: " + $scope.no);
            console.log("Roomate:: " + $scope.roommate);
            console.log("Number:: " + $scope.pro_type);
            console.log("Amenities:: " + $scope.amenities_2);
            //            console.log("StartDate:: "+$scope.startDate);            
            //            console.log("EndDate:: "+$scope.endDate);

        }

    }

    /**
        
            It Will Redirect From Index Page to Search.html
            * @param{} - pass value of location + property_type + roommate + no + amenities_2 to search
        
    */
    $scope.thik = function() {

        if (ADMIN_MODE) {

            console.log("Calling From Thik");

            console.log("StartDate:: " + $scope.startDate + "\n" + "EndDate:: " + $scope.endDate);
            //console.log($scope.loc+$scope.no);

        }

        $state.go('search', { 'location': $scope.loc, 'property': $scope.pro_type, 'roommate': $scope.roommate, 'Numbers': $scope.no, 'amenities': $scope.amenities_2, 'startDate': $scope.startDate, 'endDate': $scope.endDate, 'flag': 0 });

    }



    /**
            This function Will pass detail's from step1 to step2
           * @param{} step1 - pass value from step1
           * @callback redirect from step1 to step2
    */


    $scope.step1 = function() {


        //console.log(document.getElementById('my_address_step1').value);


        $scope.user.my_address_step1 = document.getElementById('my_address_step1').value;

        //console.log( $scope.user );

        $http.post('/users/addpropertystep1', $scope.user).then(function(data) {

            if (ADMIN_MODE) {
                console.log($scope.user);
                console.log('$scope.userdfdvd');
                console.log(data.data);
                console.log(data.data._id);
            }
            $scope.user = data;
            $state.go('property.post.head.property-step-2', { 'id': data.data._id });
        });

    }

    /**

        It will pass value from step2 to step3
        @param{} step2 - pass value from step2 to step3

    */
    $scope.step2 = function() {


        $scope.id = $stateParams.id;

        if (ADMIN_MODE) {
            console.log($scope.id);
            console.log($scope.user);
        }
        $http.post('/users/addpropertystep2/' + $scope.id, $scope.user).then(function(data) {
            if (ADMIN_MODE) {
                console.log($scope.user);
                console.log('$scope.userdfdvd');
                console.log(data.data);
                console.log(data.data._id);
            }
            $scope.user = data;
            $state.go('property.post.head.property-step-3', { 'id': data.data._id });
        });

    }


    /**
        * Pass vaue from Step3 to Step4
        * @param{} step3 - pass value from step3 to step4
        
    */
    $scope.step3 = function() {

        $scope.user.price = document.getElementById('price').value;
        $scope.id = $stateParams.id;

        if (ADMIN_MODE)
            console.log($scope.user);

        $http.post('/users/addpropertystep3/' + $scope.id, $scope.user).then(function(data) {

            $scope.user = data;


            $state.go('property.post.head.property-step-4', { 'id': data.data._id });

        });

    }

    /**
        
        * Showing Values to half-opaque container
        * @param{string} preference[]
        * @param{string} number[]
        * @param{string} type[]
        * @param{string} amenities[]
        
    */
    $scope.preference = ["Male", "Female", "No preference"];
    $scope.number = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"];
    $scope.type = ["Apartment",
        "Duplex",
        "House",
        "Townhome",
    ]
    $scope.amenities = ["All Filters",
        "Electricity",
        "Gas",
        "Water",
        "Cable",
        "WiFi",
        "Connected Bathroom",
        "Shared Bathroom",
        "Furnished",
        "Close to Transit",
        "Yard",
        "Gym",
        "Pool",
        "Pet Friendly"
    ];
    $scope.complex = ["Add a Complex", "Complex1", "Complex2", "Complex3"];
    $scope.safetyFeatures = ["Smoke Detector", "Carbon monoxide detector", "Fire Extiguinsher"];


    /**
        
        * @param{} - increase item quantity
        
    */


    $scope.increaseItemCount = function(item) {
        item.quantity++;
    }


    /**
        
        * @param{} - redirect from step4 to payumoney
        * @callback success.html
        
    */

    $scope.PayNow = function() {

        if (ADMIN_MODE)
            console.log("freferhferhihuh");
        $scope.PayArray = {

            firstName: "LOALAO",
            amount: 1800,
            desc: "this is test",
            currency: "USD"

        };

        $http.post('/users/paynow', $scope.PayArray)
            .success(function(data) {
                if (ADMIN_MODE)
                    console.log("Success:: " + data);

            }).error(function(data, status) {
                if (ADMIN_MODE)
                    console.log(data + "Status::" + status);

            });

    }



}]);




/*
     var fd=new FormData();
     var filesSelected = document.getElementById("userPhoto").files;
 
     console.log( filesSelected );
 
     fd.set('file',filesSelected[0]);
      $http.post('/users/pic', fd, {
         withCredentials: false,
         transformRequest: angular.identity,
         headers: {'Content-Type': undefined},
         enctype: 'multipart/form-data'
       })
       .success(function(response, status, headers, config) {
         console.log(response);

       });*/

/*
var filesSelected = document.getElementById("userPhoto").files;
if (filesSelected.length > 0) {
    var fileToLoad = filesSelected[0];
    var fileReader = new FileReader();
    fileReader.onload = function(fileLoadedEvent) {
    var srcData = fileLoadedEvent.target.result; // <--- data: base64
    //var newImage = document.createElement('img');*/


//        console.log(srcData);



//fileReader.readAsDataURL(fileToLoad);
//}


//}




/* 
   $scope.uploadFile=function() {

        console.log( $scope.userPhoto );

        console.log("--------Json Transformed--------");

        $http({

            method:'POST',
          url: '/users/pic',
          data: $scope.userPhoto,


        }).success(function(data) {

            console.log("Success:: " + data);

        }).error(function(data, status) {            

            console.log("Data:: " + data + "Status:: " + status);

        });


    }
     
   */
