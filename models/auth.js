module.exports = {

    'facebookAuth' : {
        'clientID'      : '1094094244037670', // your App ID
        'clientSecret'  : '20f6af15e618f4fa0d5a56ee94289332', // your App Secret
        'callbackURL'   : 'http://subleaseu.com/users/auth/facebook/callback'
    },

    'twitterAuth' : {
        'consumerKey'       : 'your-consumer-key-here',
        'consumerSecret'    : 'your-client-secret-here',
        'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
    },

    'googleAuth' : {
        'clientID'      : '143808298075-2ht61f7p4lklteqvogh43cl8nrb1dou2.apps.googleusercontent.com',
        'clientSecret'  : 'ZO118ZIF5XjdF-oYZnj-CF28',
        'callbackURL'   : 'http://subleaseu.com/users/auth/google/callback'
    }

};