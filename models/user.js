var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

mongoose.connect('mongodb://localhost/subleaseu');

var db = mongoose.connection;

// User Schema
var UserSchema = mongoose.Schema({
	
   username: {
        type: String,
        index: true
    },
    password: {
        type: String,
        bcrypt: true
    },
    email: {
        type: String,
		required: true,
		
    },
    firstname: {
        type: String
    },
	lastname: {
		type: String
	},
	dob: {
		type: String
	},
	gender: {
		type: String
	},
	contact: {
		type: String
	},
	address: {
		type: String
	},
	facebookid:{
		type:String
	},
	facebookname:{
		type:String
	},
	googleid:  {
		type: String
	},
	 provider: {
        type: String
    },
    phone: {
        type: String
    },
    preffered_language: {
        type: String
    },
    preffered_currency: {
        type: String
    },
    location: {
        type: String
    },
    describe: {
        type: String
    },
    school: {
        type: String
    },
    work: {
        type: String
    },
    timezone: {
        type: String
    },
    add_language: {
        type: String
    },
    emergency_contact: {
        type: String
    },
    shipping_add: {
        type: String
    },
    profile_pic:{
        type:String
    }
    

	
	/*facebook         : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }

});


    local            : {
       username      :String,
	   email        : String,
       password     : {type:String,bcrypt:true,required:true}
    },
    facebook         : {
        id           : String,
        name         : String
    },
    twitter          : {
        id           : String,
        token        : String,
        displayName  : String,
        username     : String
    },
    google           : {
        id           : String,
        token        : String,
        email        : String,
        name         : String
    }*/

});


var User = module.exports = mongoose.model('User', UserSchema);

module.exports.comparePassword = function(candidatePassowrd, hash, callback){
    bcrypt.compare(candidatePassowrd, hash, function(err, isMatch){
        if(err) return callback(err);
        callback(null, isMatch);
    });
}

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByUsername = function(email, callback){
    var query = {'email': email};
    User.findOne(query, callback);
	console.log(email);
}

module.exports.createUser = function(newUser,callback){
    bcrypt.hash(newUser.password, 10, function(err, hash){
        if(err) throw err;

        // Set Hashed password
        newUser.password = hash;

        // Create User
        newUser.save(callback);
    });
};

