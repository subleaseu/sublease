
var mongoose = require('mongoose');

mongoose.createConnection('mongodb://localhost/subleaseu');

var db = mongoose.connection;

// User Schema
var UserSchema = mongoose.Schema({
    
  property_type: {
       type: String,
       index: true
   },
   rooms: {
       type: String,
       bcrypt: true
   },
   beds: {
       type: String,
        required: true,
   },
   name_place:{
        type:String
   },
   describe_place:{
        type:String
   },
   guest: {
       type: String
   },
    bathrooms: {
        type: String
    },
    bathtype: {
        type: String
    },
    start_date: {
        type: Date
    },
    end_date: {
        type: Date
    },
    description: {
        type: String
    },
    minnight:{
        type:String
    },
    maxnight:{
        type:String
    },
    baseprice:  {
        type: String
    },
     houserules: {
       type: String
   },
   location: {
       type: String
   },
   listingdate: {
       type: String
   },
   preffered_currency: {
       type: String
   },
   location: {
       type: String
   },
   notification: [],
   upload_photo: []

});


module.exports = mongoose.model('listing', UserSchema);