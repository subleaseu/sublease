var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');

mongoose.createConnection('mongodb://localhost/subleaseu');

var db = mongoose.connection;

// User Schema
var UserSchema = mongoose.Schema({
	
   username: {
        type: String,
        index: true
    },
    password: {
        type: String,
        bcrypt: true
    },
    email: {
        type: String,
		required: true,
		
    }


});


var Admin= module.exports = mongoose.model('admins', UserSchema);


module.exports.comparePassword = function(candidatePassowrd, hash, callback){
    bcrypt.compare(candidatePassowrd, hash, function(err, isMatch){
        if(err) return callback(err);
        callback(null, isMatch);
    });
}

module.exports.getUserById = function(id, callback){
    User.findById(id, callback);
}

module.exports.getUserByUsername = function(email, callback){
    var query = {'email': email};
    Admin.findOne(query, callback);
	console.log(email);
}

module.exports.createUser = function(newUser,callback){
    bcrypt.hash(newUser.password, 10, function(err, hash){
        if(err) throw err;

        // Set Hashed password
        newUser.password = hash;

        // Create User
        newUser.save(callback);
    });
};

