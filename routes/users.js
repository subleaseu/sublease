var express = require('express');
var router = express.Router();
var bcrypt = require('bcryptjs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var User = require('../models/user');
var configAuth = require('../models/auth');
var flash = require('connect-flash');
var googleContacts = require('google-contacts-oauth');
var YahooStrategy = require('passport-yahoo-oauth2').OAuth2Strategy;
var request = require('request');
var outlook = require('node-outlook');
var OutlookStrategy = require('passport-outlook');
var Listing = require('../models/listing');
var Admin = require('../models/admin');
var crypto = require('crypto');
var nodemailer = require('nodemailer');
var paypal = require('paypal-rest-sdk');
var multer = require('multer');

//var fx = require('fileexpress');
//var windowslive = require('passport-windowslive');


// router.post('/uploads', pic, function(req, res)
// {
// 	upload:

// })



/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/register', function(req, res, next) {
	console.log("1");
	next();
 /* res.render('register', {
      'title': 'Register'
  });*/
});
router.get('/login', function(req, res, next) {
  /*res.render('login', {
      'title': 'Login'
  });*/
  next();
});


var isLoggedIn = function(req, res, next) {
  if (req.session && req.session.user)
    next(); // user logged in, so pass
  else
    res.redirect('/'); // not logged in, redirect to login page
};


router.post('/isLoggedIn',function(req,res)
{
    if(req.user){
        res.json(1);
    }
    else
    {
        res.json(0);
    }
}
);


router.get('/main.html', isLoggedIn, function(req, res) {
  res.send(req.session.user);
  console.log(req.session.user);
});

passport.serializeUser(function(user, done) {
  var sessionUser={_id:user._id,email:user.email,token:user.token}
  done(null, sessionUser);
  //done(null, token);
  //console.log('userrrrrrrr');
  console.log(sessionUser);
});

 passport.deserializeUser(function(req, sessionUser, done) {
      
		
            done(null, sessionUser);

       console.log(req.session.passport.user.email);
    });

passport.use(new FacebookStrategy({
	// pull in our app id and secret from our auth.js file
	//usernameField : 'email',
	clientID  :  configAuth.facebookAuth.clientID,
	clientSecret    : configAuth.facebookAuth.clientSecret,
    callbackURL     : configAuth.facebookAuth.callbackURL,
	profileFields: ['id','email', 'first_name','last_name'],
	passReqToCallback : true,
	enableProof: true
},

// facebook will send back the token and profile
    function(req,token, refreshToken, profile, done) {

        // asynchronous
        
			
			//console.log("HERE");
            //console.log(profile.id);
			
			//console.log(profile);
            // find the user in the database based on their facebook id
           /* User.findOne({ $or:[{'facebookid':profile.id },{'email':profile.emails[0].value}]}, function(err, user) {
				
				
                // if there is an error, stop everything and return that
                // ie an error connecting to the database
                if (err)
                    return done(err);

                // if the user is found, then log them in
                if (user) {
                    return done(null, user); // user found, return that user
                } else {
                    // if there is no user found with that facebook id, create them
                    var newUser            = new User();

                    // set all of the facebook information in our user model
                    newUser.facebookid    = profile.id; // set the users facebook id                     
                    newUser.firstname  = profile.name.givenName;
					newUser.lastname  = profile.name.familyName;					// look at the passport user profile to see how names are returned
                    newUser.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first
                     
                    // save our user to the database
                    newUser.save(function(err) {
                        if (err && err.name === 'ValidationError') 
						{
						err.toString().replace('ValidationError: ', '').split(',')
						}
						console.log(err);

                        // if successful, return the new user
                        return done(null, newUser);
                    });
                }

            });*/
			console.log(token);

            console.log(profile);			
			var query = {facebookid:profile.id};
			var update = {$set: {
				facebookid:profile.id,
				//email: profile.emails[0].value,
				firstname:profile.name.givenName,
				lastname:profile.name.familyName,
				provider:profile.provider
			}};
			var options = {new:true,upsert:true};
			
			User.findOneAndUpdate(query,update,options,function(err,user){
				if(err) throw err;
				return done(null,user);
			});
        
    }));
	
	
	    router.get('/auth/facebook', passport.authenticate('facebook',{scope:['email']}),function(req,res,next){});

    // handle the callback after facebook has authenticated the user
    router.get('/auth/facebook/callback',
        passport.authenticate('facebook', {failureRedirect : '/1.html'}),
            
           function(req,res,next){
			   console.log(req.session);
			  
              
			   res.redirect('/main.html');
		   } 
			
        );
		


 passport.use('google', new GoogleStrategy({
      
        clientID        : configAuth.googleAuth.clientID,
        clientSecret    : configAuth.googleAuth.clientSecret,
        callbackURL     : configAuth.googleAuth.callbackURL,
		profileFields   : ['id','emails', 'first_name','last_name','token','contacts','google'],
        passReqToCallback: true
    },
      function(req,accessToken,refreshToken, profile, done) {
		
       
           // console.log(req.session.user);
					 var query = {email: profile.emails[0].value};
			 		var update = {$set: 
				{
					googleid:profile.id,
					firstname:profile.name.givenName,
					lastname:profile.name.familyName,
					provider:profile.provider,
					token: accessToken
			 	}};
				var options = {new:true,upsert:true};
			
				User.findOneAndUpdate(query,update,options,function(err,user){
				if(err) throw err;
				return done(null,user);
				});	
				
			
        

    }));


// import contacts authentication

 passport.use('googleContactss', new GoogleStrategy({

        clientID        : '143808298075-2ht61f7p4lklteqvogh43cl8nrb1dou2.apps.googleusercontent.com',
        clientSecret    : 'ZO118ZIF5XjdF-oYZnj-CF28',
        callbackURL     : 'http://localhost:3000/users/auth/google/callback/contacts',
    profileFields   : ['id','emails', 'first_name','last_name','token','contacts','google'],
        passReqToCallback: true
    },
    function(req,accessToken,refreshToken, profile, done,res) {
    console.log('heyaaa');
    var opts = { 
      token: accessToken };

      googleContacts(opts, function(err, data){
       // var datas = JSON.stringify(data);
        // console.log(data);
		return done(null,data);
       
      })

     }));

    
        


router.get('/contacts', passport.authenticate('googleContactss', { scope :[ 'https://www.google.com/m8/feeds/contacts/default/full']}));






	 router.get('/auth/google',  passport.authenticate('google', { scope : ['email'] }));
  //router.get('/auth/google',  passport.authenticate('google', { scope : ['email', 'https://www.google.com/m8/feeds/contacts/default/full?max-results=999999&alt=json&oauth_token='] }));

    // the callback after google has authenticated the user
	//router.get('/auth/google',  passport.authenticate('google', { scope : ['email'] }));
     router.get('/auth/google/callback',
             passport.authenticate('google', {
                     successRedirect : '/main.html',
                     failureRedirect : '/'
             }));


    router.get('/auth/google/callback/contacts',
            passport.authenticate('googleContactss', {
                    successRedirect : '/main.html',
                    failureRedirect : '/'
            }));
	 
	   
  


// yahoo contacts import
/*passport.use(new YahooStrategy({
    clientID:   configAuth.yahooAuth.clientID,
    clientSecret: configAuth.yahooAuth.clientSecret,
    callbackURL:  configAuth.yahooAuth.callbackURL
},

  function(token, tokenSecret, profile, done) {
  	

    User.findOrCreate({ yahooId: profile.id }, function (err, user) {
       return done(err, user);
    });
  }
));



router.get('/auth/yahoo',
  passport.authenticate('yahoo'));

router.get('/auth/yahoo/callback',
  passport.authenticate('yahoo', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home.
    res.redirect('/');
  });
*/

// outlook import contacts
passport.use(new OutlookStrategy({
    clientID: 'd419c2ed-02c6-48f3-a61e-01040ffac945',
   // clientID: '',
    //clientSecret: configAuth.outlookAuth.clientSecret,

    clientSecret: 'Xzfmahfebtpr8efy3pdCnOm',
    callbackURL: 'https://localhost:3000/users/auth/outlook/callback'
  },
  function(accessToken, refreshToken, profile, done) {
    var user = {
      outlookId: profile.id,
      name: profile.DisplayName,
      email: profile.EmailAddress,
      accessToken:  accessToken
    };
    console.log(accessToken);
    console.log('heh');
    if (refreshToken)
      user.refreshToken = refreshToken;
    if (profile.MailboxGuid)
      user.mailboxGuid = profile.MailboxGuid;
    if (profile.Alias)
      user.alias = profile.Alias;
    User.findOrCreate(user, function (err, user) {

      return done(err, user);
    });
  }
));


router.get('/auth/outlook',
  passport.authenticate('windowslive', {
    scope: [
      'openid',
      'profile',
      'offline_access',
      'https://outlook.office.com/Mail.Read'
    ]
  })
);
 
router.get('/auth/outlook/callback', 
  passport.authenticate('windowslive', { failureRedirect: '/login' }),
  function(req, res) {
    // Successful authentication, redirect home. 
    res.redirect('/');
  });







// signup via passport

    passport.use('signup', new LocalStrategy({
   passReqToCallback : true
 },
 function(req, username, password, done) {
   findOrCreateUser = function(){
     // find a user in Mongo with provided username
     User.findOne({'email':req.body.email},function(err, user) {
       // In case of any error return
       if (err){
         console.log('Error in SignUp: '+err);
         return done(err);
       }
       // already exists
       if (user) {
         console.log('User already exists');
         return done(null, false, 
            req.flash('message','User Already Exists'));
       } else {
         // if there is no user with that email
         // create the user
         var newUser = new User();
         // set the user's local credentials
         newUser.username = req.body.username;
              newUser.email = req.body.email;
         newUser.password = createHash(req.body.password);

         console.log(req.body);
        // newUser.firstName = req.param('firstName');
        // newUser.lastName = req.param('lastName');

         // save the user
          newUser.save(function(err) {
                       if (err && err.name === 'ValidationError') 
                        {
                        err.toString().replace('ValidationError: ', '').split(',')
                        }
                        console.log(err);

                       // if successful, return the new user
                       return done(null, newUser);
                   });
       }
     });
 };
    
   // Delay the execution of findOrCreateUser and execute 
   // the method in the next tick of the event loop
   process.nextTick(findOrCreateUser);
 })
);

var createHash = function(password){
return bcrypt.hashSync(password, bcrypt.genSaltSync(10), null);
}

router.post('/register', passport.authenticate('signup', {
       successRedirect : '/', // redirect to the secure profile section
       failureRedirect : '/1.html', // redirect back to the signup page if there is an error
       failureFlash : true // allow flash messages
   }));
	
	
passport.use('local-login',new LocalStrategy( {usernameField:'email'},
    function(email, password, done){
        User.getUserByUsername(email, function(err, user){
		//	console.log(user.username);
			console.log(user.email);
            if(err) throw err;
            if(!user){
                console.log('Unknown User');
                return done(null, false, {message: 'Unknown User'});
				//var email = req.body.email;
				/*User.findOne({'email' :  email }, 
                function(err, user) {
				// In case of any error, return using the done method
				if (err)
				  return done(err);
				// Username does not exist, log error & redirect back
				if (!user){
				  console.log('User Not Found with username '+email);
				  return done(null, false, 
                req.flash('message', 'User Not found.'));   */
            }

            User.comparePassword(password, user.password, function(err, isMatch){
                if(err) throw err;
                if(isMatch){
                    return done(null, user);
                } else {
                    console.log('Invalid Password');
                    return done(null, false, {message: 'Invalid Password'});
                }
            });
        });
    }
));
	
	

	
	
	


router.post('/login', passport.authenticate('local-login',{failureRedirect: '/1.html', failureFlash: 'Invalid username or password'}), function(req, res){
    console.log('Authentication Successful');
    req.flash('success', 'You are logged in');
    res.redirect('/main.html');
});


passport.use('admin-login',new LocalStrategy( {usernameField:'email'},
    function(email, password, done){
		
		console.log(email);
        Admin.getUserByUsername(email, function(err, user){
			//console.log(user.username);
			//console.log(user.email);
            if(err) throw err;
            if(!user){
                console.log('Unknown User');
                return done(null, false, {message: 'Unknown User'});
				//var email = req.body.email;
				/*User.findOne({'email' :  email }, 
                function(err, user) {
				// In case of any error, return using the done method
				if (err)
				  return done(err);
				// Username does not exist, log error & redirect back
				if (!user){
				  console.log('User Not Found with username '+email);
				  return done(null, false, 
                req.flash('message', 'User Not found.'));   */
            }

            Admin.comparePassword(password, user.password, function(err, isMatch){
                if(err) throw err;
                if(isMatch){
                    return done(null, user);
                } else {
                    console.log('Invalid Password');
                    return done(null, false, {message: 'Invalid Password'});
                }
            });
        });
    }
));

router.post('/adminlogins', passport.authenticate('admin-login',{failureRedirect: '/1.html', failureFlash: 'Invalid username or password'}), function(req, res){
    console.log('Authentication Successful');
    req.flash('success', 'You are logged in');
    res.redirect('/Admin_Panel/dash.html');
});
	

router.get('/logout', function(req, res){
    req.logout();
    req.flash('success', 'You have logged out');
    res.redirect('/');
});

router.get('/forgot', function(req, res) {
  res.render('forgot.html', {
    user: req.user
  });
});

router.post('/forgot', function(req, res,token) {
  
    
      crypto.randomBytes(20, function(err, buf) {
        var token = buf.toString('hex');
        //done(err, token);
		console.log(token);
      
    
  console.log(token);
		var query= {'email': req.body.email};
		var update =  {'$set':{
			resetPasswordToken :token,
            resetPasswordExpires: Date.now() + 3600000 // 1 hour
		}};
		var options = {new: true, upsert: true};
      User.findOneAndUpdate(query,update,options, function(err, user) {
        if (!user) {
          req.flash('error', 'No account with that email address exists.');
          return res.redirect('/forgot.html');
        }
      })
   
    
      console.log('121324242144');
      var smtpTransport = nodemailer.createTransport( {
        service: 'Gmail',
      //  host: 'smtp.gmail.com',
		//port: 465,
		auth: {
          user: 'mohitpabnani12@gmail.com',
          pass: 'Password'
        }
      });
      var mailOptions = {
        to: req.body.email,
        from: 'passwordreset@demo.com',
        subject: 'Node.js Password Reset',
        text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
          'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
          'http://' + req.headers.host + '/users/reset/' + token + '\n\n' +
          'If you did not request this, please ignore this email and your password will remain unchanged.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        //req.flash('info', 'An e-mail has been sent to ' + req.body.email + ' with further instructions.');
		res.end('a email has been sent..');
        if(err) throw err;
      });
    
  
	  });
});


router.get('/reset/:token', function(req, res) {
  User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
	  //console.log(req.params.token);
	  //console.log(user);
	  
    if (!user) {
		
      req.flash('error', 'Password reset token is invalid or has expired.');
      return res.redirect('/forgot.html');
    }
    res.render('reset', {
      user: req.user
    });
  });
});


router.post('/reset/:token', function(req, res) {
	
	var query = {resetPasswordToken: req.params.token,resetPasswordExpires: { $gt: Date.now()} };
    var update = {password:req.body.password ,resetPasswordToken: undefined,resetPasswordExpires: undefined};
    var options = {new:true,upsert:true};
    console.log(req.params.token);
   User.findOneAndUpdate(query,update,options,function(err,user){
	   if(!user){
		   req.flash('error','token is invalid or expired');
		   return res.redirect('/forgot.html');
	   }
	   else
		   res.redirect('/');
       });
 /* async.waterfall([
    function(done) {
      User.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function(err, user) {
        if (!user) {
          req.flash('error', 'Password reset token is invalid or has expired.');
          return res.redirect('back');
        }

        user.password = req.body.password;
        user.resetPasswordToken = undefined;
        user.resetPasswordExpires = undefined;

        user.save(function(err) {
          req.logIn(user, function(err) {
            done(err, user);
          });
        });
      });
    },
    function(user, done) {
      var smtpTransport = nodemailer.createTransport('SMTP', {
        service: 'SendGrid',
        auth: {
          user: '!!! YOUR SENDGRID USERNAME !!!',
          pass: '!!! YOUR SENDGRID PASSWORD !!!'
        }
      });
      var mailOptions = {
        to: user.email,
        from: 'passwordreset@demo.com',
        subject: 'Your password has been changed',
        text: 'Hello,\n\n' +
          'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
      };
      smtpTransport.sendMail(mailOptions, function(err) {
        req.flash('success', 'Success! Your password has been changed.');
        done(err);
      });
    }
  ], function(err) {
    res.redirect('/');
  });*/
});



router.post('/newproperty',function(req,res,done){
	console.log(req.body);
	
    var document = new Listing({ email:'mp@gmail.com',
   location: req.body.my_address_step1,
   property_type: req.body.property_type,
   street: req.body.street,
   city: req.body.city,
   state: req.body.state,
   zipcode: req.body.zip,
   beds: req.body.beds,
   guest: req.body.guest,
   bathrooms: req.body.bathrooms,
   amenities: req.body.amenities,
   guest_preference: req.body.guest_preference,
   safety_features: req.body.safety_features,
   bathtype: req.body.bathtype,
   complex_name: req.body.complexname,
   complex_address: req.body.complexaddress,
   building_number: req.body.buildingnum,
   unit_number: req.body.unitnum,
   name_place: req.body.nameProperty,
   describe_place: req.body.propertyPlaceDescription,
   upload_photo: req.body.photo,
   set_price: req.body.price,
   start_date: req.body.startdate,
   end_date: req.body.enddate,
   conditions: req.body.condition,
   cancellation: req.body.refundTime
      });
	   document.save(function(err,records){
		if(err) throw err;
		console.log(records);
		res.end();
		});
   })



// router.post('/pic', function(req, res){

//     console.log(req.body.photoJ[0]['base64']);
    
    
// //    var writeme=req.body.photoJ;
// //    
// //        require("fs").writeFile('out.txt',writeme, function(err){
// //
// //             console.log(err);
// //
// //         });
// //    
// //    var imagearr = [];
// //    
// //    for(z=0;z<req.body.length;z++)
// //    {
// //
// //        var base64string = req.body[z]['base64'];
// //        var image =  './public/upload/' + Date.now()+ z + '.jpeg';
// //          imagearr.push(image);
// //
// //         console.log(imagearr);
// //
// //         require("fs").writeFile(image, base64string, 'base64', function(err){
// //
// //             console.log(err);
// //
// //         })
// //
// //    }

// //  Listing.find({email:req.session.passport.user.email},{upload_photo:1}, function(err,paths)
// //   {
// //     
// //     if(!paths)
// //     {
// //       var newArray =[];
// //        for(i=0;i<req.body.length;i++)
// //        {
// //           newArray = req.body[i].path;
// //
// //        }
// //       console.log(req.session.passport.user.email); 
// //       var query = {'email': require.session.passport.user.email};
// //         var update = {$set:{
// //       upload_photo: newArray
// //       }};
// //         var options = {new: true, upsert: true}
// //         
// //    
// //        Listing.findOneAndUpdate(query,update,options, function(err)
// //        {
// //         if(err) throw(err);
// //        })
// //     } 
// //     
// //     else
// //       {
// //         console.log('-----------');
// //           console.log(imagearr);
// //           
// //         console.log('-----------');
// //               
// //           var query = {'email': req.session.passport.user.email};
// //           
// //
// //                
// //                  
// //              var update = { $push: { upload_photo: { $each: imagearr } } };
// //               
// //
// //         var options = {new: true, upsert: true}
// //         Listing.findOneAndUpdate(query,update,options, function(err)
// //        {
// //         if(err) throw(err);
// //        })
// //        
// //     } 
// //     res.end();
// //               
// //        
// // })
// //  
  
// });




router.post('/pic', function(req, res){


  //console.log(req.body.photoJ[0]['base64']);

 var imagearr = [];
 //console.log(req.body.photoJ[0]['base64']);
console.log(req.body.photoJ.length);
 for( z=0;z<req.body.photoJ.length;z++ )
 {

   var base64string = req.body.photoJ[z]['base64'];
    
    // console.log(req.session.passport.user._id);
   // console.log(base64string);
   
    var image =  './public/upload/'+ req.session.passport.user._id + Date.now()+ z + '.jpeg';
     
      var str =  image.slice(9);
      
      //console.log(str);
      
      imagearr.push(str);

      //imagearr.push(image);
     // console.log(imagearr);
   
     require("fs").writeFile(image, base64string, 'base64', function(err){
     console.log(err);
   
   })

 }

  Listing.find({email:req.session.passport.user.email},{upload_photo:1}, function(err,paths)
   {
     
     if(!paths)
     {
       var newArray =[];
        for(i=0;i<req.body.length;i++)
        {
           newArray = req.body.photoJ[i].path;

        }
        
       var query = {'_id': req.params.id};
         var update = {$set:{
       upload_photo: newArray
       }};
         var options = {new: true, upsert: true}

        Listing.findOneAndUpdate(query,update,options, function(err)
        {
         if(err) throw(err);
        })
     } 
     
     else
       {
         console.log('-----------');
           
           console.log(imagearr);
           
         console.log('-----------');
               
           var query = {'email': req.session.passport.user.email};
           

                
                  
              var update = { $push: { upload_photo: { $each: imagearr } } };
               

         var options = {new: true, upsert: true}
         Listing.findOneAndUpdate(query,update,options, function(err)
        {
         if(err) throw(err);
        })
        
     } 
     
               
        
 })
  res.end();
});





router.post('/profilepics', function(req, res){


  var base64string=req.body['data'];
   
  
    console.log(base64string);
  
   var image =  './public/profileupload/'+ req.session.passport.user._id  + '.jpeg';
    
     var str =  image.slice(9);
     // imagearr =[];
     
     //console.log(str);
     
     // 11111imagearr.push(str);

     //imagearr.push(image);
    // console.log(imagearr);
  
    require("fs").writeFile(image, base64string, 'base64', function(err){
    console.log(err);
  
  })

// }

 Listing.find({email:req.session.passport.user._id},{profile_pic:1}, function(err,paths)
  {
    
    if(!paths)
    {
      var newArray =[];
       for(i=0;i<req.body.length;i++)
       {
          newArray = req.body.photoJ[i].path;

       }
       
      var query = {'_id': req.params.id};
        var update = {$set:{
      upload_photo: newArray
      }};
        var options = {new: true, upsert: true}

       Listing.findOneAndUpdate(query,update,options, function(err)
       {
        if(err) throw(err);
       })
    } 
    
    else
      {
        console.log('-----------');
          
          // console.log(imagearr);
          
        console.log('-----------');
              
          var query = {'email': req.session.passport.user._id};
         

               
                 
             var update = { '$set': { profile_pic: str } };
             

        var options = {new: true, upsert: true}
        Listing.findOneAndUpdate(query,update,options, function(err)
       {
        if(err) throw(err);
       })
       
    } 
    
              
       
})
 res.end();


});



 router.post('/addpropertystep1', function(req,res){
   
   //res.setHeader('Content-Type', 'application/x-www-form-urlencoded');
 
 console.log(req.body);
  console.log(req.session.passport);
 var query= {'email': req.session.passport.user.email};
 var update =  {'$set':{
   location: req.body.my_address_step1,
   property_type: req.body.property_type,
   street: req.body.street,
   city: req.body.city,
   state: req.body.state,
   zipcode: req.body.zip,
   beds: req.body.bedCount,
   guest: req.body.guest,
   bathrooms: req.body.bathroomCount,
   amenities: req.body.amenities,
   guest_preference: req.body.guest_preference,
   safety_features: req.body.safety_features,
   bathtype: req.body.bathtype,
   complex_name: req.body.complexname,
   complex_address: req.body.complexaddress,
   building_number: req.body.buildingnum,
   unit_number: req.body.unitnum

   
   
 }};
  
   console.log('hey');
   
   var options = {new: true, upsert: true};
   Listing.findOneAndUpdate(query,update,options, function(err,user){
      if(err) throw(err);
    console.log(err);
   
    //  return done(null,user);
      res.json(user);
	  console.log(user);
 
   })
 });
 
 router.post('/addpropertystep2/:id', function(req, res, done){

  console.log(req.body)
  console.log(req.params.id);
  var query = {'_id': req.params.id};
  console.log(query);
  var update = {$set:{
   name_place: req.body.nameProperty,
   describe_place: req.body.propertyPlaceDescription
   
  }};

  var options ={new: true, upsert: true};
  Listing.findOneAndUpdate(query,update,options, function(err,user){
    if(err)
      throw(err);
    console.log(err);
	
	res.json(user);

  }) 


 });

 router.post('/addpropertystep3/:id', function(req, res, done){

     console.log(req.body);
     var query = {'_id':req.params.id};
     var update = {$set:{
         set_price: req.body.price,
         start_date: req.body.startdate,
         end_date: req.body.enddate,
         conditions: req.body.condition,
         cancellation: req.body.refundTime
   }};

   var options = {new: true, upsert: true};
   Listing.findOneAndUpdate(query,update,options, function(err,user){
    if(err) throw(err);
    console.log(err);
	res.json(user);

   })
 

});








 router.post('/editprofile', function(req, res,done){
 console.log(req.body);
 console.log(req.body[0].firstname);
 var query= {email: req.session.passport.user.email};
 var update =  {$set: {
   firstname: req.body[0].firstname,
   lastname: req.body[0].lastname,
   gender: req.body[0].gender,
   dob: req.body[0].dob,
   email: req.body[0].email,
   phone: req.body[0].phone,
   preffered_language: req.body[0].language,
   preffered_currency: req.body[0].currency,
   location: req.body[0].location,
   describe: req.body[0].describe,
   school: req.body[0].school,
   work: req.body[0].work,
   timezone: req.body[0].timezone,
   add_language: req.body[0].addlanguage,
   emergency_contact: req.body[0].emergency,
   shipping_add: req.body[0].shipping
   
    }};
    
   console.log('heyaaaaaaaa');
   var options = {new: true, upsert: true};
   User.findOneAndUpdate(query,update,options, function(err, user){
      if(err) throw(err);
   console.log('find');
   //console.log(user);
      //return done(null,user);
	  //var users= user;
	  res.end();
  


   });

 });


 router.post('/getlistingBar',function(req,res,done){
     //	 console.log(req.body);
     //	 console.log(req.body.location);
	 
     //     var location= req.body.location;
     //	 var property= req.body.property;
     //	 var roommate= req.body.roommate;
     //	 var amenities= req.body.amenities;
     
	 //var location= req.params.location;
	// console.log(property);
	 Listing.find({'location':req.body.location}, function(err,listings){
		 if(err) throw err;
		 		 else if(listings){
					 
					 console.log(listings);
			 
			 res.json(listings)
	         }
		 else 
			 return done(null,false,req.flash('message','No Listing Found'));

	 })
 });



 router.post('/getlisting',function(req,res,done){

     
      
     console.log(req.body);
     console.log(req.body.location);
     console.log(req.body.property);
     console.log(req.body.startDate);
     console.log(req.body.endDate);

	 
     //  var location= req.body.location;
     //	 var property= req.body.property;
     //	 var roommate= req.body.roommate;
     //	 var amenities= req.body.amenities;
	 //var location= req.params.location;
	 //console.log(property);
     
	 Listing.find({'location':req.body.location,'start_date':{$lte:req.body.startDate},'end_date':{$gte:req.body.endDate}},function(err,listings){
         
		 if(err) throw err;
		 		 else if(listings){
					 
					 console.log(listings);
			 
			 res.json(listings)
	         }
		 else 
      res.json("0");
			 return done(null,false,req.flash('message','No Listing Found'));

	 })
 });
 
 router.post('/showlisting/:id',function(req,res,done){
	 console.log(req.body);
	 var id= req.params.id;
	 console.log(id);
	 Listing.findById(id, function(err,listings){
		 if(err) throw err;
		 		 else if(listings){
					 
					 console.log(id);
			 
			 res.json(listings)
	         }
		 else 
			 return done(null,false,req.flash('message','No Listing Found'));

	 })
 });
 
 router.post('/getyourlisting',function(req,res,done){
	 //console.log(req.body);
	 
	 //var query = {'email': req.session.passport.user.email};
 Listing.find({},function(err,listing){
		 if(err) throw err;
		 		 else if(listing){
					 //console.log(email);
					 //console.log(req.body.location);
			
			 res.json(listing);
			 console.log(listing);
	         }
		 else 
			 return done(null,false,req.flash('message','No Listing Found'));

	 })
 });

  router.post('/myproperty', function(req,res,done){
    var query = {'email': req.session.passport.user.email};

    Listing.find(query,function(err,user){
      if(err) throw err;
      else if(user){
        console.log(user);
        res.setHeader('Content-Type', 'application/json');
        res.json(user);
      }
      else
        return done(null);
     });

  });

 router.post('/getdetails',function(req,res,done){
	 //console.log(req.body);
	 
	 var query = {'email': req.session.passport.user.email};
 User.find(query,function(err,user){
		 if(err) throw err;
		 		 else if(user){
					 //console.log(email);
					 //console.log(req.body.location);
			res.setHeader('Content-Type', 'application/json');
           res.json(user);
			// res.json(user);
			// console.log(user);
	         }
		 else 
			 return done(null,false,req.flash('message','No Listing Found'));

	 })
 });
 
paypal.configure({
  'mode': 'sandbox', //sandbox or live 
  'client_id': 'AWQOQaIWctkEKw0pZ0itdu0JplkLUyWgrzxMEi_xPYFoqS7G8BmurCQGSxtNU89_Hf2MpRro0-auJ72F',
  'client_secret': 'ELaflhQsH4uAqTvvZN6wDSIobRpyTCVyoeevMKq8RC5akoqQOM53EuznRsY_XOHTP4Mw9oODkCZpkgjP'
});
 
// Page will display after payment has beed transfered successfully
router.get('/success', function(req, res) {
  res.send("Payment transfered successfully.");
});
 
// Page will display when you canceled the transaction 
router.get('/cancel', function(req, res) {
  res.send("Payment canceled successfully.");
});
 
router.post('/paynow', function(req, res) {
   // paypal payment configuration.
    
  var payment = {
  "intent": "sale",
  "payer": {
    "payment_method": "paypal"
  },
  "redirect_urls": {
    "return_url": "http://localhost:3000/users/success",
    "cancel_url": "http://localhost:3000/users/cancel"
  },
  "transactions": [{
    "amount": {
      "total":2560.00,
      "currency":'USD'
    },
    "description": 'This is Test'
  }]
};
 
  paypal.payment.create(payment, function (error, payment) {
  if (error) {
      console.log("HOLA1");
    console.log(error);
            console.log("HOLA2");

  } else {
    if(payment.payer.payment_method === 'paypal') {
      req.paymentId = payment.id;
      var redirectUrl;
      console.log(payment);
      for(var i=0; i < payment.links.length; i++) {
        var link = payment.links[i];
        if (link.method === 'REDIRECT') {
          redirectUrl = link.href;
        }
      }
      res.redirect(redirectUrl);
    }
  }
});
});


router.post('/getusers',function(req,res,done){
     //console.log(req.body);
     
     //var query = {'email': req.session.passport.user.email};
User.find({},function(err,user){
         if(err) throw err;
                  else if(user){
                     //console.log(email);
                     //console.log(req.body.location);
            
             res.json(user);
             console.log(user);
             }
         else 
             return done(null,false,req.flash('message','No user Found'));

     })
});

/*router.post('/notification/:id',function(req,res,done){
	
	var notification = [];
	
	var query = {'_id':req.params.id};
     var update = {$set:{
       // $push:{notification:req.session.passport.user.email}
	   notification:req.session.passport.user.email

   }};

   var options = {new: true, upsert: true};
   Listing.findOneAndUpdate(query,update,options, function(err,user){
    if(err) throw(err);
    console.log(err);
	res.json(user);

   })
 
	 
})
*/

router.post('/notification/:id',function(req,res){
	
	var notification = [];
	
	var query = {'_id':req.params.id};
     
  
   Listing.findOne(query, function(err,user){
   // if(err) throw(err);
    //console.log(err);
	console.log(user.notification);
	
	console.log(user.notification.length);
	
	if(user.notification)
	{
		/*for(i=0;i<user.notification.length;i++)
		{
			var notification = req.session.passport.user.email;
		}*/
		//console.log(i);
		
		 var update = {
        $push:{notification:req.session.passport.user.email}

       };
	   
	   Listing.findOneAndUpdate(query,update,function(err)
	   {
		   if(err) throw err;
		   console.log('lalallllalala');
	   })
		//var list = new Listing(user);
		/*user.save(function(err){
			console.log('lalalalalalal');
			if(err) 
			console.log(err);
		})*/
		
	}
	
	else
    {
		//res.end('hollaalla');
		var update = {
        $push:{notification:req.session.passport.user.email}

       };
	   
	   Listing.findOneAndUpdate(query,update,function(err)
	   {
		   if(err) throw err;
		   console.log('else part');
	   })
		console.log('ollallal');
	}
	res.json(user);

   })
 
	 
})

router.post('/notificationcount',function(req,res)
{
Listing.findOne({'email':req.session.passport.user.email},function(err,user)
	{
		console.log(user.notification);
	})
	res.end();
})


module.exports = router;
