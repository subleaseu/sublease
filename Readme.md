Official Sublease U Project Designed By Appwallaz

This Site Provides Property,Apartment on Lease

Requirement:

    1. Good Internet Connection
    2. NodeJs
    3. MongoDB
    4. Text Editor(Recommended Brackets)
    
How to Use (Windows User) :

    1. First Pull or clone the project using Git you can use Git_Bash For That.
    2. After cloning the project press SHIFT + Right Click and Select "Open command window here" that will open cmd on that folder
    3. Now type node app.js the server will start
    4. if you see "server running at port 3000" that means server has run successfully
    5. Open Web Browser and type " localhost:3000 "
    6. That's It